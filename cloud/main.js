
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response) {
  response.success("Hello world!");
});


// Algolia Functions
// Goes Here

var Algolia = require('cloud/algoliasearch-node');
var algolia  = new Algolia('8OLGAV9I02', 'fc4bae9de5e39788278b625992ac24c2');

// initialize Profile class as index Object
var index   = algolia.initIndex('Profile');
// initialize FamilySearch class as family_search Object
var family_search   = algolia.initIndex('FamilySearch');




/** After Save Function saves FamilySearch to Algolia's FamilySearch from Parse's FamilySearch **/
Parse.Cloud.afterSave("FamilySearch", function(request) {
  //Convert Parse.Object to JSON
  var parseObject = request.object.toJSON();

  //Override Algolia object ID with the Parse.Object unique ID
  parseObject.objectID = parseObject.objectId;

  //Add new family_search (if no matching parseObject.objectID) or update
  family_search.saveObject(parseObject, function(error, content) {
    if (error)
     console.log('Got an error: ' + content.message);
  });  
  request.success(request);
});



/** After Save Function saves Cotacts to Algolia's Contacts from Parse's Contacts **/
Parse.Cloud.afterSave("Contacts", function(request) {
  //Convert Parse.Object to JSON
  var parseObject = request.object.toJSON();

  //Override Algolia object ID with the Parse.Object unique ID
  parseObject.objectID = parseObject.objectId;

  //Add new index (if no matching parseObject.objectID) or update
  index.saveObject(parseObject, function(error, content) {
    if (error)
     console.log('Got an error: ' + content.message);
  });  
  request.success(request);
});


          
// After Delete Functions
// Goes Here

/** After Delete Function deletes FamilySearch record from Algolia's FamilySearch, Which are deleted from Parse's FamilySearch **/
Parse.Cloud.afterDelete('FamilySearch', function(request) {
  //Get the Parse/Algolia objectId
  var objectId = request.object.id;

  //Remove the index from Algolia
  family_search.deleteObject(objectId, function(error, content) {
    if (error)
      console.log('Got an error: ' + content.message);
  });
});            
              
/** After Delete Function deletes Contacts record from Algolia's Contacts, Which are deleted from Parse's Contacts **/
Parse.Cloud.afterDelete('Contacts', function(request) {
  //Get the Parse/Algolia objectId
  var objectId = request.object.id;

  //Remove the index from Algolia
  index.deleteObject(objectId, function(error, content) {
    if (error)
      console.log('Got an error: ' + content.message);
  });
});






// Before Save parts goes here

var FamilySearch = Parse.Object.extend("FamilySearch");

// Check if person_id is set, and enforce uniqueness based on the person_id column.
Parse.Cloud.beforeSave("FamilySearch", function(request, response) {
  if (!request.object.get("person_id")) {
    response.error('A FamilySearch must have a person_id.');
  } else {
    var query = new Parse.Query(FamilySearch);
    query.equalTo("person_id", request.object.get("person_id"));
    query.first({
      success: function(object) {
        if (object) {
          response.error("A FamilySearch with this person_id already exists.");
        } else {
          response.success();
        }
      },
      error: function(error) {
        response.error("Could not validate uniqueness for this FamilySearch object.");
      }
    });
  }
});
